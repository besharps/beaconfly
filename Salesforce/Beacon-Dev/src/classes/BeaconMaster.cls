@RestResource(urlMapping='/beaconsFly')
global class BeaconMaster {
    @HttpPost
    global static void doPost() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        string payload = req.requestBody.toString();
        system.debug(payload);
		RequestWrapper wrapper = (RequestWrapper)JSON.deserialize(payload, RequestWrapper.class);
        
        Flyby__c fb = new Flyby__c();
        fb.PhoneId__c = wrapper.phoneId;
        fb.BeaconId__c = wrapper.beaconId;
        fb.Range__c =   double.valueOf(wrapper.range.replace('m', '').replace('c', '').trim());
        fb.Range_Indicator__c = wrapper.rangeIndicator;
        
        
        Contact c = new Contact();
        c.PhoneId__c = wrapper.phoneId;
        fb.Contact__r = c;
        insert fb;
        
        
    }
    
    public class RequestWrapper 		
    {
        public String phoneId {get;set;}
        public String beaconId {get;set;}
        public String range {get;set;}
        public String rangeIndicator {get;set;}
    }
}