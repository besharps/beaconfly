app.utils.notifications = {

    notify: function(title, message){
        cordova.plugins.notification.local.hasPermission(function(granted) {
            if (granted == true) {
                app.utils.notifications.schedule(title, message);
            }
            else {
                cordova.plugins.notification.local.registerPermission(function(granted) {
                    if (granted == true) {
                        app.utils.notifications.schedule(title, message);
                    }
                    else {
                        navigator.notification.alert("Reminder cannot be added because app doesn't have permission");
                    }
                });
            }
        });
    },

    schedule: function(title, message) {
        cordova.plugins.notification.local.schedule({
            id: 1,
            title: title,
            message: message,
            at: new Date(),
            icon: 'file://images/icon.png'
        });
    }
};