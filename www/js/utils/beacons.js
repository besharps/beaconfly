app.utils.beacons = {

    proximityNames: [
        'unknown',
        'immediate',
        'near',
        'far'
    ],

    beaconColorStyles : [
        'style-color-unknown style-color-unknown-text',        // BeaconColorUnknown
        'style-color-mint style-color-mint-text',              // BeaconColorMintCocktail
        'style-color-ice style-color-ice-text',                // BeaconColorIcyMarshmallow
        'style-color-blueberry-dark style-color-blueberry-dark-text', // BeaconColorBlueberryPie
        'style-color-unknown style-color-unknown-text',        // TODO: BeaconColorSweetBeetroot
        'style-color-unknown style-color-unknown-text',        // TODO: BeaconColorCandyFloss
        'style-color-unknown style-color-unknown-text',        // TODO: BeaconColorLemonTart
        'style-color-unknown style-color-unknown-text',        // TODO: BeaconColorVanillaJello
        'style-color-unknown style-color-unknown-text',        // TODO: BeaconColorLiquoriceSwirl
        'style-color-white style-color-white-text',            // BeaconColorWhite
        'style-color-transparent style-color-transparent-text' // BeaconColorTransparent
    ],

    regions : [
            {
                identifier: 'Blue',
                uuid:'b9407f30-f5f8-466e-aff9-25556b57fe6d',
                major: '26166',
                minor: '24728'
            }
    ],

    formatDistance : function(meters)
    {
        if (!meters) { return 'Unknown'; }

        if (meters > 1)
        {
            return meters.toFixed(3) + ' m';
        }
        else
        {
            return (meters * 100).toFixed(3) + ' cm';
        }
    },

    formatProximity : function(proximity)
    {
        if (!proximity) { return 'Unknown'; }

        // Eliminate bad values (just in case).
        proximity = Math.max(0, proximity);
        proximity = Math.min(3, proximity);

        // Return name for proximity.
        return this.proximityNames[proximity];
    },

    beaconColorStyle: function(color)
    {
        if (!color)
        {
            color = 0;
        }

        // Eliminate bad values (just in case).
        color = Math.max(0, color);
        color = Math.min(5, color);

        // Return style class for color.
        return this.beaconColorStyles[color];
    }
};