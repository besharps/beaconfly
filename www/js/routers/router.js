app.routers.AppRouter = Backbone.Router.extend({

    routes: {
        "": "home",
        "range": "range",
        "monitor": "monitor"
    },

    home: function () {
        var homeView = new app.views.HomeView();

        homeView.render();
    },

    range: function() {
        var beaconCollection = new app.collections.Beacons();

        var rangeView = new app.views.RangeView({
            collection: beaconCollection
        });

        rangeView.render();
    },

    monitor: function() {
        var regionState = new app.models.RegionState();

        var monitorView = new app.views.MonitorView({
            model: regionState
        });

        monitorView.render();
    }
});
