app.views.HomeView = Backbone.View.extend({

    el: "#content",

    initialize: function() {

        _.bindAll(this, 'render');

        // Request authorisation.
        estimote.beacons.requestAlwaysAuthorization();

        // Start monitoring.
        $.each(app.utils.beacons.regions, function(index, region){
            estimote.beacons.startMonitoringForRegion(
                region,
                this.onMonitor,
                this.onError);
        }.bind(this));
    },

    render: function() {

        this.$el.html(this.template());

        adjustMenu();

        return this;
    },

    onMonitor: function(regionState)
    {
        var payload = {
            "phoneId": device.uuid,
            "beaconId": regionState.uuid + '.' + regionState.major + '.' + regionState.minor,
            "range": 0,
            "rangeIndicator": regionState.state
        };

        $.ajax({
            url: 'https://beaconsfly-developer-edition.na30.force.com/services/apexrest/beaconsFly',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            processData: false,
            success: function (data) {
                var dataAsJSON = JSON.stringify(data);
                if(dataAsJSON !== ''){
                    app.utils.notifications.notify('BeaconFly', dataAsJSON);
                }
            },
            error: function (request, status, error) {
                alert('Error ' + request.responseText);
            },
            data: JSON.stringify(payload)
        });
    },

    onError: function(errorMessage)
    {
        console.log('Monitor error: ' + errorMessage);
    }
});
