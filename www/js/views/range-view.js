app.views.RangeView = Backbone.View.extend({

    el: "#content",

    range: 0,

    events: {
        'click #back-btn': 'stopRangingBeacons'
    },

    initialize: function() {
        var self = this;

        _.bindAll(this, 'render', 'onRange');


        this.collection.bind('change reset sync add remove', function() {
            self.render();
        });

        // Request authorisation.
        estimote.beacons.requestAlwaysAuthorization();

        // Start ranging.
        estimote.beacons.startRangingBeaconsInRegion(
            {}, // Empty region matches all beacons.
            this.onRange,
            this.onError);
    },

    render: function() {

        this.$el.html(this.template({
            beacons: this.collection.toJSON()
        }));

        return this;
    },

    onRange: function(beaconInfo) {
        this.collection.reset(beaconInfo.beacons);

        if ((this.range % 5) == 0) {

            $.each(beaconInfo.beacons, function (key, beacon) {
                //TODO remove "m"
                var payload = {
                    "phoneId": device.uuid,
                    "beaconId": beacon.major + '.' + beacon.minor,
                    "range": beacon.distance.toFixed(3),
                    "rangeIndicator": app.utils.beacons.formatProximity(beacon.proximity)
                };

                $.ajax({
                    url: 'https://beaconsfly-developer-edition.na30.force.com/services/apexrest/beaconsFly',
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    processData: false,
                    success: function (data) {
                        //alert('Done ' + data);
                    },
                    error: function (request, status, error) {
                        //alert('Error ' + request.responseText);
                    },
                    data: JSON.stringify(payload)
                });
            });
        }

        this.range++;
    },

    onError: function(errorMessage) {
        console.log('Range error: ' + errorMessage);
    },

    stopRangingBeacons : function()
    {
        estimote.beacons.stopRangingBeaconsInRegion({});
        app.router.navigate("#", true);
    }
});
