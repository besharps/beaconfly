app.views.MonitorView = Backbone.View.extend({

    el: "#content",

    events: {
        'click #back-btn': 'stopMonitoringBeacons'
    },

    initialize: function() {
        var self = this;

        _.bindAll(this, 'render', 'onMonitor');

        this.model.bind('change sync add remove', function() {
            self.render();
        });

        // Request authorisation.
        estimote.beacons.requestAlwaysAuthorization();

        // Start monitoring.
        $.each(app.utils.beacons.regions, function(index, region){
            estimote.beacons.startMonitoringForRegion(
                region,
                this.onMonitor,
                this.onError);
        }.bind(this));
    },

    render: function() {
        this.$el.html(this.template({
            regionState: this.model.toJSON()
        }));

        return this;
    },

    onMonitor: function(regionState)
    {
        this.model.set(regionState);
        app.utils.notifications.notify('BeaconFly', regionState.state);
    },

    onError: function(errorMessage)
    {
        console.log('Monitor error: ' + errorMessage);
    },

    stopMonitoringRegions : function()
    {
        estimote.beacons.stopMonitoringForRegion({});
        app.router.navigate("#", true);
    }
});
