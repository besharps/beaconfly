app.models.Beacon = Backbone.Model.extend({

    initialize : function(attributes, options){
        this.set("style", app.utils.beacons.beaconColorStyle(attributes.color));
        this.set("formattedProximity", app.utils.beacons.formatProximity(attributes.proximity));
        this.set("formattedDistance", app.utils.beacons.formatDistance(attributes.distance));
    }
});
