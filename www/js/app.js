// Define application object and common functions.
var app = {

	routers: {},
	models: {},
	collections: {},
	views: {},
	utils: {},

	initialize: function() {
		this.bindEvents();
	},

	bindEvents: function() {
		document.addEventListener('deviceready', this.onDeviceReady, false);
	},

	onDeviceReady: function() {
		app.utils.templates.load(["HomeView",
									"RangeView",
									"MonitorView"], function() {

			app.router = new app.routers.AppRouter();

			Backbone.history.start();
		});
	}
};
