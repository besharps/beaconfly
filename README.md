# BeaconFly #

### Setup ###

* [Phonegap Desktop Download (GUI Version)](https://github.com/phonegap/phonegap-app-desktop/releases/download/0.3.3/PhoneGapSetup-win32.exe)
* [Node.js Download for NPM](https://nodejs.org/dist/v4.4.7/win-x86/node.exe)
* CMD: npm install -g phonegap (CLI Version)
* CMD in the projet folder: phonegap platform add android
* CMD in the projet folder: phonegap platform add ios

### Android ###
* CMD in the projet folder: phonegap build android
* CMD in the projet folder: phonegap run android (enable USB debugging on your phone before running this)

### iOS ###
* CMD in the projet folder: phonegap build ios

### Phonegap Serve / Phonegap mobile app / Browser ###
* Estimote SDK doesnt work need to compile
* CMD in the projet folder: phonegap serve

### SDK Manager ###
* Android Support Repository
* Google Repository
* Google USB Driver

### Download
https://build.phonegap.com/apps/2221459/download/android/?qr_key=5_qhLf1qSi-miezsPcxm